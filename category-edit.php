<?php 
require 'dbcon.php';
include "includes/header.php";
session_start();
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Category Edit</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Category Edit
                        <a href="view_all_categories.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 

                        if(isset($_GET['cat_id'])) {
                            $the_cat_id = $_GET['cat_id'];
                        }

                        $query ="SELECT * FROM categories WHERE cat_id = '$the_cat_id'";
                        $select_cat_by_id = mysqli_query($con, $query);

                        while ($row = mysqli_fetch_assoc($select_cat_by_id)) {
                            $cat_id = $row['cat_id'];
                            $cat_title = $row['cat_title'];
                        }


                        if(isset($_POST['update_category'])) {

                            $cat_id = mysqli_real_escape_string($con, $_POST['cat_id']);
                            $cat_title = mysqli_real_escape_string($con, $_POST['cat_title']);

                            $query = "UPDATE categories SET cat_title= '{$cat_title}' WHERE cat_id='$the_cat_id'";
                            $query_run = mysqli_query($con, $query);
                            if($query_run) {

                                $_SESSION['message'] = "Category Updated Successfully";
                                header("Location: view_all_categories.php");
                                exit(0);

                            } else {

                                $_SESSION['message'] = "Category Not Updated";
                                header("Location: category-edit.php");
                                exit(0);

                            }

                        }

                 

                        ?>



                                <form action="" method="POST">
                                    <input type="hidden" name="cat_id" value="<?php echo $cat_id; ?>">

                                <div class="mb-3">
                                    <label>Category Title</label>
                                    <input type="text" name="cat_title" value="<?php echo $cat_title; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <button type ="submit" name="update_category" class="btn btn-primary">Edit Category</button>
                                </div>
                            
                                </form>
                             
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    -->
  </body>
</html>