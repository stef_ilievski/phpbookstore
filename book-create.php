<?php 
session_start();
include "dbcon.php";
include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Add new book</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

    <?php 

    if(isset($_POST['save_book'])) {
    $book_title =  $_POST['book_title'];
    $book_desc =  $_POST['book_description'];
    $book_author_id =  $_POST['book_author_id'];
    $book_isbn =  $_POST['book_isbn'];

    $book_image = $_FILES['book_image']['name'];
    $book_image_temp = $_FILES['book_image']["tmp_name"];

    $book_format =  $_POST['book_format'];
    $book_language =  $_POST['book_language'];
    $book_publisher = $_POST['book_publisher'];
    $book_category_id = $_POST['book_category_id'];

    move_uploaded_file($book_image_temp, "images/$book_image");

    $query = "INSERT INTO books (book_title, book_description, book_author_id, book_isbn, book_image, book_format, book_language, book_publisher, book_category_id) 
    VALUES ('{$book_title}', '{$book_desc}', '{$book_author_id}', '{$book_isbn}', '{$book_image}', '{$book_format}', '{$book_language}', '{$book_publisher}', {$book_category_id})";

    $query_run = mysqli_query($con, $query);
 
    if($query_run) {
        $_SESSION['message'] = "Book Created Successfully";
        header("Location: book-create.php");
        exit(0);
    } else {
        $_SESSION['message'] = "Book Not Created";
        header("Location: book-create.php");
        exit(0);
    }
}

?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Add Book
                        <a href="view_all_books.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="" method="POST" enctype="multipart/form-data">

                        <div class="mb-3">
                            <label>Book Title</label>
                            <input type="text" name="book_title" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Book Description</label>
                            <input type="text" name="book_description" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Book Author</label>
                            <br>
                            <select name="book_author_id" id="book_author_id">
                           
                           <?php 
                            
                             $query = "SELECT * FROM authors";
                             $select_authors = mysqli_query($con, $query);

                             while($row = mysqli_fetch_assoc($select_authors)) {
                             $author_id = $row['author_id'];
                             $author_name = $row['author_name'];

                             echo "<option value='{$author_id}'>{$author_name}</option>";
                

                            }
                            

                            ?>


                            </select>
                        </div>
                        <div class="mb-3">
                            <label>Book ISBN</label>
                            <input type="text" name="book_isbn" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label for="book_image">Book Image</label>
                            <input type="file" name="book_image" id="book_image"/>
                        </div>
                        <div class="mb-3">
                            <label>Book Format</label>
                            <input type="text" name="book_format" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Book Language</label>
                            <input type="text" name="book_language" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Book Publisher</label>
                            <input type="text" name="book_publisher" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Book Category</label>
                            <br>
                            <select name="book_category_id" id="book_category_id">
                           
                           <?php 
                            
                             $query = "SELECT * FROM categories";
                             $select_categories = mysqli_query($con, $query);

                             while($row = mysqli_fetch_assoc($select_categories)) {
                             $cat_id = $row['cat_id'];
                             $cat_title = $row['cat_title'];

                             echo "<option value='{$cat_id}'>{$cat_title}</option>";
                

                            }
                            

                            ?>


                            </select>
                        </div>

                        <div class="mb-3">
                            <button type ="submit" name="save_book" class="btn btn-primary">Save Book</button>
                        </div>
                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
    </script>                        

  </body>
</html>