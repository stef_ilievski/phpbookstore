<?php 
session_start();
include "dbcon.php";
include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>New User</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

    <?php 

    if(isset($_POST['save_user'])) {
        $username = $_POST['username'];
        $user_firstname = $_POST['user_firstname'];
        $user_lastname = $_POST['user_lastname'];
        $user_email = $_POST['user_email'];
        $user_password = $_POST['user_password'];

    $query = "INSERT INTO users (username, user_firstname, user_lastname, user_email, user_password) VALUES ('{$username}', '{$user_firstname}', '{$user_lastname}', '{$user_email}', '{$user_password}')";    
    $query_run = mysqli_query($con, $query);
 
    if($query_run) {
        $_SESSION['message'] = "User Created Successfully";
        header("Location: user-create.php");
        exit(0);
    } else {
        $_SESSION['message'] = "User Not Created :(";
        header("Location: user-create.php");
        exit(0);
    }
}

?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Add User
                        <a href="view_all_users.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">

                        <div class="mb-3">
                            <label>Username</label>
                            <input type="text" name="username" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>First Name</label>
                            <input type="text" name="user_firstname" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Last Name</label>
                            <input type="text" name="user_lastname" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Email</label>
                            <input type="email" name="user_email" class="form-control">
                        </div>
                        <div class="mb-3">
                            <label>Password</label>
                            <input type="password" name="user_password" class="form-control">
                        </div>

                        <div class="mb-3">
                            <button type ="submit" name="save_user" class="btn btn-primary">Save User</button>
                        </div>
                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    -->
  </body>
</html>