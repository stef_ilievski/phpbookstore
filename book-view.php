<?php 
require 'dbcon.php';
//include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>View Book</title>
  </head>
  <body>

    <div class="container mt-5">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Book Details
                        <a href="view_all_books.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 
                        if(isset($_GET['book_id'])) {
                            $the_book_id = mysqli_real_escape_string($con, $_GET['book_id']);
                        }

                            $query ="SELECT * FROM books WHERE book_id='$the_book_id'";
                            $query_run = mysqli_query($con, $query);

                            while($row = mysqli_fetch_assoc($query_run)) {
                                
                                $book_id = $row['book_id'];
                                $book_title = $row['book_title'];
                                $book_desc = $row['book_description'];
                                $book_author = $row['book_author_id'];
                                $book_isbn = $row['book_isbn'];
                                $book_image = $row['book_image'];
                                $book_format = $row['book_format'];
                                $book_language = $row['book_language'];
                                $book_publisher = $row['book_publisher'];
                                $book_category = $row['book_category_id'];
                            }
                                    ?>

                                    <div class="mb-3">
                                        <label>Title</label>
                                        <p class="form-control">
                                        <?php echo $book_title; ?>
                                        </p>
                                    </div>
                                    <div class="mb-3">
                                        <label>Description</label>
                                        <p class="form-control">
                                        <?php echo $book_desc; ?>
                                        </p>
                                    </div>

                                    <?php 
                                    
                                    $query = "SELECT * FROM authors WHERE author_id = '{$book_author}'";
                                    $select_authors = mysqli_query($con, $query);

                                    while($row = mysqli_fetch_assoc($select_authors)) {
                                    $author_id = $row['author_id'];
                                    $author_name = $row['author_name'];
                        

                                    }
                                    

                                    ?>

                                    <div class="mb-3">
                                        <label>Author</label>
                                        <p class="form-control">
                                        <?php echo $author_name; ?>
                                        </p>
                                    </div>
                                    <div class="mb-3">
                                        <label>ISBN</label>
                                        <p class="form-control">
                                        <?php echo $book_isbn; ?>
                                        </p>
                                    </div>
                                    <div class="mb-3">
                                        <label>Image</label>
                                        <img width="100" class="book_image" src="images/<?php echo $book_image; ?>">
                                       
                                    </div>
                                    <div class="mb-3">
                                        <label>Format</label>
                                        <p class="form-control">
                                        <?php echo $book_format; ?>
                                        </p>
                                    </div>
                                    <div class="mb-3">
                                        <label>Language</label>
                                        <p class="form-control">
                                        <?php echo $book_language; ?>
                                        </p>
                                    </div>
                                    <div class="mb-3">
                                        <label>Publisher</label>
                                        <p class="form-control">
                                        <?php echo $book_publisher;?>
                                        </p>
                                    </div>

                                    
                                    <?php 
                                    $query = "SELECT * FROM categories WHERE cat_id = '{$book_category}'";
                                    $update_categories = mysqli_query($con, $query);

                                    while($row = mysqli_fetch_assoc($update_categories)) {
                                        $cat_id = $row['cat_id'];
                                        $cat_title = $row['cat_title'];
                                    }
                                     ?>
                                    
                                    
                                    <div class="mb-3">
                                        <label>Category</label>
                                        <p class="form-control">
                                        <?php echo $cat_title; ?>
                                        </p>
                                    </div>
                                
                        
                             
                                    

                              
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php 
                    
                    if(isset($_POST['create_review'])) {

                        $the_book_id =  $_GET['book_id'];

                        $review_author = $_POST['review_author'];
                        $review_content = $_POST['review_content'];

                        if(!empty($review_author) && !empty($review_content)) {

                        
                            $query = "INSERT INTO reviews (review_book_id, review_author, review_content, review_date) VALUES ($the_book_id, '{$review_author}', '{$review_content}', NOW())";
                            $create_review = mysqli_query($con, $query);
    
                            if(!$create_review) {
                                die("Review Query Failed" . mysqli_error($con));
                            }
    
                            $query = "UPDATE books SET book_review_count = book_review_count + 1 WHERE book_id = $the_book_id";
                            $update_review_count = mysqli_query($con, $query);
    
                             
                        } else {
                            echo "<script>alert('Fields cannot be empty')</script>";
                        }


                        }

                          
                       


                    ?>

    <div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4>Leave a review:</h4></div>
                    <div class="card-body">
                    <form action="" method="post" role="form">

                        <div class="form-group">
                            <label for="Author">Author</label>
                            <input type="text" class="form-control" name="review_author">
                        </div>

                        <div class="form-group">
                            <label for="Comment">Comment</label>
                            <textarea class="form-control" name="review_content" rows="3"></textarea>
                        </div>
                        <button type="submit" name="create_review" class="btn btn-primary">Submit</button>
                    </form>
                    </div>
                </div>
             </div>            
    </div>



   <?php 
                           
                    $query = "SELECT * FROM reviews WHERE review_book_id = {$the_book_id} ORDER BY review_id DESC ";
                    $select_reviews_query = mysqli_query($con, $query);
                    if(!$select_reviews_query) {
                        die("Query failed" . mysqli_error($con));
                    }
                    while($row = mysqli_fetch_assoc($select_reviews_query)) {
                        $review_date = $row['review_date'];
                        $review_content = $row['review_content'];
                        $review_author = $row['review_author'];

                        ?>


<div class="container mt-5">
        <div class="row">
            <div class="col-md-12">
                <div class="card-header"><h4><?php echo $review_author; ?></h4></div>
                    <div class="card-body">
                    <form action="" role="form">
                        <div class="form-group">
                            <label for="Date"><?php echo $review_date; ?></label>
                        </div>

                        <div class="form-group">
                            <textarea disabled class="form-control" name="review_content" rows="3"><?php echo $review_content; ?></textarea>
                        </div>
                    </form>
                    </div>
                </div>
             </div>            
    </div>


                  <?php   }

                    ?>
  
</body>
</html>