<?php 
session_start();
include "dbcon.php";
include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https:cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

    <?php 

    if(isset($_POST['save_author'])) {
    $author_name = mysqli_real_escape_string($con, $_POST['author_name']);

    $query = "INSERT INTO authors (author_name) VALUES ('{$author_name}')";

    $query_run = mysqli_query($con, $query);
  
    if($query_run) {
        $_SESSION['message'] = "Author Created Successfully";
        header("Location: view_all_authors.php");
        exit(0);
    } else {
        $_SESSION['message'] = "Category Not Created";
        header("Location: author-create.php");
        exit(0);
    }
}

?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Author Add
                        <a href="view_all_authors.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <form action="" method="POST">

                        <div class="mb-3">
                            <label>Author Name</label>
                            <input type="text" name="author_name" class="form-control">
                        </div>
                        <div class="mb-3">
                            <button type ="submit" name="save_author" class="btn btn-primary">Save Author</button>
                        </div>
                       
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https:cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    -->
  </body>
</html>