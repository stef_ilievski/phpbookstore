<?php 
require 'dbcon.php';
session_start();
include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Book Edit</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Book Edit
                        <a href="view_all_books.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 

                        if(isset($_GET['book_id'])) {
                        $the_book_id = $_GET['book_id'];
                        }

                        $query ="SELECT * FROM books WHERE book_id='$the_book_id'";
                        $select_books_by_id = mysqli_query($con, $query);

                        while($row = mysqli_fetch_assoc($select_books_by_id)) {
                            $book_title = $row['book_title'];
                            $book_desc = $row['book_description'];
                            $book_author = $row['book_author_id'];
                            $book_isbn = $row['book_isbn'];
                            $book_image = $row['book_image'];
                            $book_format = $row['book_format'];
                            $book_language = $row['book_language'];
                            $book_publisher = $row['book_publisher'];
                            $book_category = $row['book_category_id'];
                        }
                        

                        if(isset($_POST['update_book'])) {

                            $book_id = $_POST['book_id'];
                            $book_title = $_POST['book_title'];
                            $book_desc = $_POST['book_description'];
                            $book_author = $_POST['book_author_id'];
                            $book_isbn = $_POST['book_isbn'];
                            $book_image = $_FILES['book_image']['name'];
                            $book_image_temp = $_FILES['book_image']['tmp_name'];
                            $book_format = $_POST['book_format'];
                            $book_language = $_POST['book_language'];
                            $book_publisher = $_POST['book_publisher'];
                            $book_category = $_POST['book_category_id'];

                            move_uploaded_file($book_image_temp, "images/$book_image");
                            

                            $query = "UPDATE books SET book_title ='{$book_title}', book_description = '{$book_desc}', book_author_id = '{$book_author}', book_isbn = '{$book_isbn}', book_image = '{$book_image}', book_format = '{$book_format}',
                            book_language = '{$book_language}', book_publisher = '{$book_publisher}', book_category_id = '{$book_category}' WHERE book_id = '$the_book_id'";
                            $update_books_query = mysqli_query($con, $query);
                            if($update_books_query) {

                                $_SESSION['message'] = "Book Updated Successfully";
                                header("Location: view_all_books.php");
                                exit(0);

                            } else {

                                $_SESSION['message'] = "Book Not Updated";
                                header("Location: view_all_books.php");
                                exit(0);

                            }

                        }

                      

                            

                            
                                ?>



                                <form action="" method="POST" enctype="multipart/form-data">
                                    <input type="hidden" name="book_id" value="<?php echo $book_id; ?>">
                                <div class="mb-3">
                                    <label>Title</label>
                                    <input type="text" name="book_title" value="<?php echo $book_title; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Description</label>
                                    <input type="text" name="book_description" value="<?php echo $book_desc; ?>"class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Book Author</label>
                                    <br>
                                    <select name="book_author_id" id="book_author_id">
                                
                                <?php 
                                    
                                    $query = "SELECT * FROM authors";
                                    $select_authors = mysqli_query($con, $query);

                                    while($row = mysqli_fetch_assoc($select_authors)) {
                                    $author_id = $row['author_id'];
                                    $author_name = $row['author_name'];

                                    echo "<option value='{$author_id}'>{$author_name}</option>";
                        

                                    }
                                    

                                    ?>


                                    </select>
                                </div>
                                <div class="mb-3">
                                    <label>ISBN</label>
                                    <input type="text" name="book_isbn" value="<?php echo $book_isbn; ?>" class="form-control">
                                </div>
                                <div class="form-group">
                                    <img width="100px" src="images/<?php echo $book_image; ?>" alt="">
                                    <input type="file" name="book_image" id="book_image">
                                </div>
                                <div class="mb-3">
                                    <label>Format</label>
                                    <input type="text" name="book_format" value="<?php echo $book_format; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Language</label>
                                    <input type="text" name="book_language" value="<?php echo $book_language; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Publisher</label>
                                    <input type="text" name="book_publisher" value="<?php echo $book_publisher; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                            <label>Book Category</label>
                            <br>
                            <select name="book_category_id" id="book_category_id">
                           
                           <?php 
                            
                             $query = "SELECT * FROM categories";
                             $select_categories = mysqli_query($con, $query);

                             while($row = mysqli_fetch_assoc($select_categories)) {
                             $cat_id = $row['cat_id'];
                             $cat_title = $row['cat_title'];

                             echo "<option value='{$cat_id}'>{$cat_title}</option>";
                

                            }
                            

                            ?>


                            </select>
                        </div>
                                <div class="mb-3">
                                    <button type ="submit" name="update_book" class="btn btn-primary">Edit Book</button>
                                </div>
                            
                                </form>
                                <?php 
                                    

                                                      
                            ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

  </body>
</html>