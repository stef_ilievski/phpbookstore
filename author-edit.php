<?php 
require 'dbcon.php';
include "includes/header.php";
session_start();
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Category Edit</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Author Edit
                        <a href="view_all_authors.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 

                        if(isset($_GET['author_id'])) {
                            $the_author_id = $_GET['author_id'];
                        }

                        $query ="SELECT * FROM authors WHERE author_id = '$the_author_id'";
                        $select_author_by_id = mysqli_query($con, $query);

                        while ($row = mysqli_fetch_assoc($select_author_by_id)) {
                            $author_id = $row['author_id'];
                            $author_name = $row['author_name'];
                        }


                        if(isset($_POST['update_author'])) {

                            $author_id = mysqli_real_escape_string($con, $_POST['author_id']);
                            $author_name = mysqli_real_escape_string($con, $_POST['author_name']);

                            $query = "UPDATE authors SET author_name= '{$author_name}' WHERE author_id='$the_author_id'";
                            $query_run = mysqli_query($con, $query);
                            if($query_run) {

                                $_SESSION['message'] = "Author Updated Successfully";
                                header("Location: view_all_authors.php");
                                exit(0);

                            } else {

                                $_SESSION['message'] = "Author Not Updated";
                                header("Location: author-edit.php");
                                exit(0);

                            }

                        }

                 

                        ?>



                                <form action="" method="POST">
                                    <input type="hidden" name="author_id" value="<?php echo $author_id; ?>">

                                <div class="mb-3">
                                    <label>Author Name</label>
                                    <input type="text" name="author_name" value="<?php echo $author_name; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <button type ="submit" name="update_author" class="btn btn-primary">Edit Author</button>
                                </div>
                            
                                </form>
                             
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    -->
  </body>
</html>