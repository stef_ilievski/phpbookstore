<?php 
require 'dbcon.php';
include "includes/header.php";
session_start();
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>User Edit</title>
  </head>
  <body>

    <div class="container mt-5">

    <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>User Edit
                        <a href="view_all_users.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 

                        if(isset($_GET['user_id'])) {
                            $the_user_id = $_GET['user_id'];
                        }

                        $query ="SELECT * FROM users WHERE user_id = '$the_user_id'";
                        $select_user_by_id = mysqli_query($con, $query);

                        while ($row = mysqli_fetch_assoc($select_user_by_id)) {
                            $user_id = $row['user_id'];
                            $username = $row['username'];
                            $user_firstname = $row['user_firstname'];
                            $user_lastname = $row['user_lastname'];
                            $user_email = $row['user_email'];
                            $user_password = $row['user_password'];
                        }


                        if(isset($_POST['update_user'])) {

                            $user_id = mysqli_real_escape_string($con, $_POST['user_id']);
                            $username = mysqli_real_escape_string($con, $_POST['username']);
                            $user_firstname = mysqli_real_escape_string($con, $_POST['user_firstname']);
                            $user_lastname = mysqli_real_escape_string($con, $_POST['user_lastname']);
                            $user_email = mysqli_real_escape_string($con, $_POST['user_email']);
                            $user_password = mysqli_real_escape_string($con, $_POST['user_password']);

                            $query = "UPDATE users SET username = '{$username}', user_firstname = '{$user_firstname}', user_lastname = '{$user_lastname}', user_email = '{$user_email}', user_password = '{$user_password}' WHERE user_id='$the_user_id'";
                            $query_run = mysqli_query($con, $query);
                            if($query_run) {

                                $_SESSION['message'] = "User Updated Successfully";
                                header("Location: view_all_users.php");
                                exit(0);

                            } else {

                                $_SESSION['message'] = "User Not Updated";
                                header("Location: user-edit.php");
                                exit(0);

                            }

                        }

                 

                        ?>



                                <form action="" method="POST">
                                    <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">

                                <div class="mb-3">
                                    <label>Username</label>
                                    <input type="text" name="username" value="<?php echo $username; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>First Name</label>
                                    <input type="text" name="user_firstname" value="<?php echo $user_firstname ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Last Name</label>
                                    <input type="text" name="user_lastname" value="<?php echo $user_lastname; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Email</label>
                                    <input type="email" name="user_email" value="<?php echo $user_email; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <label>Password</label>
                                    <input type="text" name="user_password" value="<?php echo $user_password; ?>" class="form-control">
                                </div>
                                <div class="mb-3">
                                    <button type ="submit" name="update_user" class="btn btn-primary">Edit User</button>
                                </div>
                            
                                </form>
                             
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    -->
  </body>
</html>