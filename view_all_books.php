<?php 

session_start();
require 'dbcon.php';


?>

<?php include ('includes/header.php'); ?>



    <div class="container mt-5">

        <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Book Details
                            <a href="book-create.php" class="btn btn-primary float-end">Add Book</a>
                        </h4>
                    </div>
                    <div class="card-header">
                        <h4>All Authors
                            <a href="view_all_authors.php" class="btn btn-primary float-end">View All Authors</a>
                        </h4>
                    </div>
                    <div class="card-header">
                        <h4>All Categories
                            <a href="view_all_categories.php" class="btn btn-primary float-end">View All Categories</a>
                        </h4>
                    </div>
                    <div class="card-header">
                        <h4>Current User: <?php echo $_SESSION['username']; ?>
                        <a href='logout.php'><i class='fa fa-fw fa-power-off'></i>Log Out</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Author</th>
                                    <th>ISBN</th>
                                    <th>Image</th>
                                    <th>Format</th>
                                    <th>Language</th>
                                    <th>Publisher</th>
                                    <th>Category</th>
                                    <th>Action</th>
                                  
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $query = "SELECT * FROM books";
                                $select_books = mysqli_query($con, $query);

                               

                                    while($row = mysqli_fetch_assoc($select_books)) {
                                    $book_id = $row['book_id'];
                                    $book_title = $row['book_title'];
                                    $book_desc = $row['book_description'];
                                    $book_author = $row['book_author_id'];
                                    $book_isbn = $row['book_isbn'];
                                    $book_image = $row['book_image'];
                                    $book_format = $row['book_format'];
                                    $book_language = $row['book_language'];
                                    $book_publisher = $row['book_publisher'];
                                    $book_category = $row['book_category_id'];

                                        
                                        
                                echo "<tr>";
                                    echo "<td>{$book_id}</td>";
                                    echo "<td>{$book_title}</td>";
                                    echo "<td>{$book_desc}</td>";

                                    $query = "SELECT * FROM authors WHERE author_id = {$book_author}";
                                    $update_authors = mysqli_query($con, $query);

                                    while($row = mysqli_fetch_assoc($update_authors)) {
                                        $author_id = $row['author_id'];
                                        $author_name = $row['author_name'];
                                    }
                                    echo "<td><a href='author_books.php?author_id={$author_id}&author_name={$author_name}&book_id={$book_id}'>{$author_name}</a></td>";    

                                    
                                    echo "<td>{$book_isbn}</td>";     
                                   // echo "<td><img width='100' id='book_image' alt='image'></td>";                           
                                    echo "<td><img width='100' src='images/{$book_image}' alt='image'></td>";
                                    echo "<td>{$book_format}</td>";
                                    echo "<td>{$book_language}</td>";
                                    echo "<td>{$book_publisher}</td>";

                                    $query = "SELECT * FROM categories WHERE cat_id = {$book_category}";
                                    $update_categories = mysqli_query($con, $query);

                                    while($row = mysqli_fetch_assoc($update_categories)) {
                                        $cat_id = $row['cat_id'];
                                        $cat_title = $row['cat_title'];
                                    }
                                    echo "<td><a href='category_books.php?cat_id={$cat_id}&cat_title={$cat_title}&book_id={$book_id}'>{$cat_title}</a></td>";
                                   
                                  echo  "<td>";
                                       echo "<a href='book-view.php?book_id={$book_id}'class='btn btn-info btn-sm'>View</a>";
                                       echo "<a href='book-edit.php?book_id={$book_id}'class='btn btn-success btn-sm'>Edit</a>";
                                        echo "<form action='' method='POST' class='d-inline'>";
                                        echo "<button type='submit' name='delete_book' value='{$book_id}' class='btn btn-danger btn-sm'>Delete</button>";
                                       echo "</form>";
                                    
                                    echo "</td>";
                                echo "</tr>";
                          
                                    }

                                
                                ?>

                                <?php 
                                
                                if(isset($_POST['delete_book'])) {

                                    $book_id = mysqli_real_escape_string($con, $_POST['delete_book']);
                                
                                    $query = "DELETE FROM books WHERE book_id='$book_id'";
                                    $query_run=mysqli_query($con, $query);
                                
                                    if($query_run) {
                                
                                        $_SESSION['message'] = "Book Deleted Successfully";
                                        header("Location: view_all_books.php");
                                        exit(0);
                                
                                    } else { 
                                
                                        $_SESSION['message'] = "Book Not Deleted";
                                        header("Location: view_all_books.php");
                                        exit(0);
                                
                                    }
                                
                                } 

                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include ('includes/footer.php'); ?>


