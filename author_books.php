<?php 

//include "includes/header.php";
include "dbcon.php";

?>

<?php 
            if(isset($_GET['book_id'])) {

                $the_book_id =  $_GET['book_id'];
                $the_book_author =  $_GET['author_id'];
                $the_book_author_name = $_GET['author_name'];

            }   

?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">


    <title>Book Store</title>
  </head>
  <body>

<div class="container mt-5">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <h4>All Books by: <a href='author-view.php?author_id=<?php echo $the_book_author; ?>'><?php echo $the_book_author_name; ?></a>
                    <a href="view_all_books.php" class="btn btn-danger float-end">Go Back</a>
                </h4>
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>   
                            </tr>
                        </thead>
                        <tbody>
                            <?php              
                        $query = "SELECT * FROM books WHERE book_author_id = '{$the_book_author}'";
                        $select_all_books_query = mysqli_query($con, $query);
        
        
                        while ($row = mysqli_fetch_assoc($select_all_books_query)) { 
                                $book_id = $row['book_id'];
                                $book_title = $row['book_title'];
                                $book_desc = $row['book_description'];
                                $book_format = $row['book_format'];

                                    
                                    
                            echo "<tr>";
                                echo "<td>{$book_title}</td>";
                                echo "<td>{$book_desc}</td>";
                            echo "</tr>";
                    
                                }

                            
                            ?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
    
<?php 

include "includes/footer.php";

?>
