<?php 

session_start();
require 'dbcon.php';


?>

<?php include ('includes/header.php'); ?>

    <div class="container mt-5">

        <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Author Details
                            <a href="author-create.php" class="btn btn-primary float-end">Add Author</a>
                        </h1>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $query = "SELECT * FROM authors";
                                $select_categories = mysqli_query($con, $query);

    
                                    while($row = mysqli_fetch_assoc($select_categories)) {
                                    $author_id = $row['author_id'];
                                    $author_name = $row['author_name'];
                      

                                        
                                        
                                echo "<tr>";
                                    echo "<td>{$author_id}</td>";
                                    echo "<td>{$author_name}</td>";        
                                  echo  "<td>";
                                       echo "<a href='author-view.php?author_id={$author_id}'class='btn btn-info btn-sm'>View</a>";
                                       echo "<a href='author-edit.php?author_id={$author_id}'class='btn btn-success btn-sm'>Edit</a>";
                                        echo "<form action='' method='POST' class='d-inline'>";
                                        echo "<button type='submit' name='delete_author' value='{$author_id}' class='btn btn-danger btn-sm'>Delete</button>";
                                       echo "</form>";
                                    echo "</td>";
                                echo "</tr>";
                                        
                                    }

                                
                                ?>

                                <?php 
                                
                                if(isset($_POST['delete_author'])) {

                                    $the_author_id = mysqli_real_escape_string($con, $_POST['delete_author']);
                                
                                    $query = "DELETE FROM authors WHERE author_id='$the_author_id'";
                                    $query_run=mysqli_query($con, $query);
                                
                                    if($query_run) {
                                
                                        $_SESSION['message'] = "Author Deleted Successfully";
                                        header("Location: view_all_authors.php");
                                        exit(0);
                                
                                    } else { 
                                
                                        $_SESSION['message'] = "Author Not Deleted";
                                        header("Location: view_all_authors.php");
                                        exit(0);
                                
                                    }
                                
                                } 

                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include ('includes/footer.php'); ?>


