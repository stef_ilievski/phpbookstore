<?php 

session_start();
require 'dbcon.php';


?>

<?php include ('includes/header.php'); ?>

    <div class="container mt-5">

        <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Category Details
                            <a href="category-create.php" class="btn btn-primary float-end">Add Category</a>
                        </h1>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $query = "SELECT * FROM categories";
                                $select_categories = mysqli_query($con, $query);

    
                                    while($row = mysqli_fetch_assoc($select_categories)) {
                                    $cat_id = $row['cat_id'];
                                    $cat_title = $row['cat_title'];
                      

                                        
                                        
                                echo "<tr>";
                                    echo "<td>{$cat_id}</td>";
                                    echo "<td>{$cat_title}</td>";        
                                  echo  "<td>";
                                       echo "<a href='category-view.php?cat_id={$cat_id}'class='btn btn-info btn-sm'>View</a>";
                                       echo "<a href='category-edit.php?cat_id={$cat_id}'class='btn btn-success btn-sm'>Edit</a>";
                                        echo "<form action='' method='POST' class='d-inline'>";
                                        echo "<button type='submit' name='delete_cat' value='{$cat_id}' class='btn btn-danger btn-sm'>Delete</button>";
                                       echo "</form>";
                                    echo "</td>";
                                echo "</tr>";
                                        
                                    }

                                
                                ?>

                                <?php 
                                
                                if(isset($_POST['delete_cat'])) {

                                    $the_cat_id = mysqli_real_escape_string($con, $_POST['delete_cat']);
                                
                                    $query = "DELETE FROM categories WHERE cat_id='$the_cat_id'";
                                    $query_run=mysqli_query($con, $query);
                                
                                    if($query_run) {
                                
                                        $_SESSION['message'] = "Category Deleted Successfully";
                                        header("Location: view_all_categories.php");
                                        exit(0);
                                
                                    } else { 
                                
                                        $_SESSION['message'] = "Category Not Deleted";
                                        header("Location: view_all_categories.php");
                                        exit(0);
                                
                                    }
                                
                                } 

                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include ('includes/footer.php'); ?>


