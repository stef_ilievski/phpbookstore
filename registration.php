<?php  include "dbcon.php"; ?>

 <?php 
 
 if(isset($_POST['submit'])) {
    $username = $_POST['username'];
    $firstname = $_POST['user_firstname'];
    $lastname = $_POST['user_lastname'];
    $email = $_POST['user_email'];
    $password = $_POST['user_password'];

    if(!empty($username) && !empty($firstname) && !empty($lastname) && !empty($email) && !empty($password)) {

    $query = "INSERT INTO users (username, user_firstname, user_lastname, user_email, user_password, user_role) VALUES ('{$username}', '{$firstname}', '{$lastname}', '{$email}', '{$password}', 'subscriber') ";
    $register_user = mysqli_query($con, $query);

    if(!$register_user) {
        die("Query Failed" . mysqli_error($con));
    }

    echo "Successfully Registered";
    
} else { 
    echo "<h3 class='text-center'>Please fill in all the fields!</h3>";
}

 }

 ?>

    
 
<!doctype html>
<html lang="en">
  <head>
  	<title>Register</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/style.css">

	</head>
	<body class="img js-fullheight" style="background-image: url(images/books.jpg);">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Enter your details</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="login-wrap p-0">
		      	<form action="" class="signin-form" method="post">
		      		<div class="form-group">
		      			<input type="text" name="username" class="form-control" placeholder="Username" required>
		      		</div>
                      <div class="form-group">
		      			<input type="text" name="user_firstname" class="form-control" placeholder="First Name" required>
		      		</div>
                      <div class="form-group">
		      			<input type="text" name="user_lastname" class="form-control" placeholder="Last Name" required>
		      		</div>
                      <div class="form-group">
		      			<input type="email" name="user_email" class="form-control" placeholder="Email" required>
		      		</div>
	            <div class="form-group">
	              <input id="password-field" type="password" name="user_password" class="form-control" placeholder="Password" required>
	              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
	            </div>
	            <div class="form-group">
	            	<button type="submit" name="submit" class="form-control btn btn-primary submit px-3">Register</button>
	            </div>
	            <div class="form-group d-md-flex">
	            	<div class="w-50">
								</div>
								<div class="w-50 text-md-right">
									<a href="#" style="color: #fff"></a>
								</div>
	            </div>
	          </form>
	          <p class="w-100 text-center">&mdash; Have an account? &mdash;</p>
	          <div class="social d-flex text-center">
	          	<a href="login.php" class="px-2 py-2 mr-md-1 rounded"><span class="ion-logo-facebook mr-2"></span> Login Here</a>
	          </div>
		      </div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>

	</body>
</html>
