<?php 
require 'dbcon.php';
include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>User Edit</title>
  </head>
  <body>

    <div class="container mt-5">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>User Details
                        <a href="view_all_users.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 
                        if(isset($_GET['user_id'])) {
                            $the_user_id = mysqli_real_escape_string($con, $_GET['user_id']);
                        }

                            $query = "SELECT * FROM users WHERE user_id = '$the_user_id'";
                            $query_run = mysqli_query($con, $query);

                            while($row = mysqli_fetch_assoc($query_run)) {
                                
                                $user_id = $row['user_id'];
                                $username = $row['username'];
                                $user_firstname = $row['user_firstname'];
                                $user_lastname = $row['user_lastname'];
                                $user_email = $row['user_email'];
                                $user_password = $row['user_password'];
                       
                            }
                                    ?>

                                    <div class="mb-3">
                                        <label>Username</label>
                                        <p class="form-control">
                                        <?php echo $username; ?>
                                        </p>
                                    </div>  
                                    <div class="mb-3">
                                        <label>First Name</label>
                                        <p class="form-control">
                                        <?php echo $user_firstname; ?>
                                        </p>
                                    </div>  
                                    <div class="mb-3">
                                        <label>Last Name</label>
                                        <p class="form-control">
                                        <?php echo $user_lastname; ?>
                                        </p>
                                    </div>  
                                    <div class="mb-3">
                                        <label>Email</label>
                                        <p class="form-control">
                                        <?php echo $user_email; ?>
                                        </p>
                                    </div>  
                                    <div class="mb-3">
                                        <label>Password</label>
                                        <p class="form-control">
                                        <?php echo $user_password; ?>
                                        </p>
                                    </div>                             
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    
  </body>
</html>