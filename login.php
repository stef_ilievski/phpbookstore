<?php include "dbcon.php"; ?>
<?php session_start(); ?>

<?php 

if (isset($_POST['login'])) {

 $username = $_POST['username'];
 $password = $_POST['user_password'];


 $username = mysqli_real_escape_string($con, $username);
 $password = mysqli_real_escape_string($con, $password);

 $query = "SELECT * FROM users WHERE username = '{$username}' AND user_password = '{$password}' ";
 $login_user_query = mysqli_query($con, $query);

 if(!$login_user_query) {
    die("Login Query Failed" . mysqli_error($con));
 } 

 while($row = mysqli_fetch_assoc($login_user_query)) {
    $db_user_id = $row['user_id'];
    $db_username = $row['username'];
    $db_user_password = $row['user_password'];
    $db_user_firstname = $row['user_firstname'];
    $db_user_lastname = $row['user_lastname'];
    $db_user_role = $row['user_role'];
 }

 
 
 if($username === $db_username && $password === $db_user_password) {

   $_SESSION['username'] = $db_username;
   $_SESSION['user_firstname'] = $db_user_firstname;
   $_SESSION['user_lastname'] = $db_user_lastname;
   $_SESSION['user_role'] = $db_user_role;

   header("Location: view_all_books.php");

 } else {
    
    header("Location: registration.php");
 }



}

?>


<!-- Login 
<div class="well">
    <h4>Login</h4>
    <form action="login.php" method="post">
    <div class="form-group">
        <input name="username" type="text" class="form-control" placeholder="Enter Username">
        <input name="user_password" type="password" class="form-control" placeholder="Enter Password">
        </div>
        <span class="input-group-btn">
            <button class="btn btn-primary" name="login" type="submit">Login</button>
        </span>
    </form>
</div>
-->

<!doctype html>
<html lang="en">
  <head>
  	<title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700&display=swap" rel="stylesheet">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	
	<link rel="stylesheet" href="css/style.css">

	</head>
	<body class="img js-fullheight" style="background-image: url(images/bookstore.jpg);">
	<section class="ftco-section">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-6 text-center mb-5">
					<h2 class="heading-section">Bookstore Login</h2>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-md-6 col-lg-4">
					<div class="login-wrap p-0">
		      	<h3 class="mb-4 text-center">Have an account?</h3>
		      	<form action="" class="signin-form" method="post">
		      		<div class="form-group">
		      			<input type="text" name="username" class="form-control" placeholder="Username" required>
		      		</div>
                    
	            <div class="form-group">
	              <input id="password-field" type="password" name="user_password" class="form-control" placeholder="Password" required>
	              <span toggle="#password-field" class="fa fa-fw fa-eye field-icon toggle-password"></span>
	            </div>
	            <div class="form-group">
	            	<button type="submit" name="login" class="form-control btn btn-primary submit px-3">Sign In</button>
	            </div>
	            <div class="form-group d-md-flex">
	            	<div class="w-50">
								</div>
								<div class="w-50 text-md-right">
									<a href="#" style="color: #fff"></a>
								</div>
	            </div>
	          </form>
	          <p class="w-100 text-center">&mdash; Don't have an account? &mdash;</p>
	          <div class="social d-flex text-center">
	          	<a href="registration.php" class="px-2 py-2 mr-md-1 rounded"><span class="ion-logo-facebook mr-2"></span> Register Here</a>
	          </div>
		      </div>
				</div>
			</div>
		</div>
	</section>

	<script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>

	</body>
</html>

