<?php 
require 'dbcon.php';
//include "includes/header.php";
?>


<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Category Details</title>
  </head>
  <body>

    <div class="container mt-5">

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Category Details
                        <a href="view_all_books.php" class="btn btn-danger float-end">Go Back</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        
                        <?php 
                        if(isset($_GET['cat_id'])) {
                            $the_cat_id = mysqli_real_escape_string($con, $_GET['cat_id']);
                        }

                            $query ="SELECT * FROM categories WHERE cat_id='$the_cat_id'";
                            $query_run = mysqli_query($con, $query);

                            while($row = mysqli_fetch_assoc($query_run)) {
                                
                                $cat_id = $row['cat_id'];
                                $cat_title = $row['cat_title'];
                       
                            }
                                    ?>

                                    <div class="mb-3">
                                        <label>Title</label>
                                        <p class="form-control">
                                        <?php echo $cat_title; ?>
                                        </p>
                                    </div>                             
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    
  </body>
</html>