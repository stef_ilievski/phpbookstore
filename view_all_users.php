<?php 

session_start();
require 'dbcon.php';


?>

<?php include ('includes/header.php'); ?>

    <div class="container mt-5">

        <?php include('message.php'); ?>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4>User Details
                            <a href="user-create.php" class="btn btn-primary float-end">Add User</a>
                        </h1>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Username</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Action</th>
                      
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                $query = "SELECT * FROM users";
                                $select_users = mysqli_query($con, $query);

    
                                    while($row = mysqli_fetch_assoc($select_users)) {
                                        $user_id = $row['user_id'];
                                        $username = $row['username'];
                                        $user_firstname = $row['user_firstname'];
                                        $user_lastname = $row['user_lastname'];
                                        $user_email = $row['user_email'];
                                        $user_password = $row['user_password'];

                      

                                        
                                        
                                echo "<tr>";
                                    echo "<td>{$user_id}</td>";
                                    echo "<td>{$username}</td>";
                                    echo "<td>{$user_firstname}</td>";     
                                    echo "<td>{$user_lastname}</td>";     
                                    echo "<td>{$user_email}</td>";     
                                    echo "<td>{$user_password}</td>";             
                                  echo  "<td>";
                                       echo "<a href='user-view.php?user_id={$user_id}'class='btn btn-info btn-sm'>View</a>";
                                       echo "<a href='user-edit.php?user_id={$user_id}'class='btn btn-success btn-sm'>Edit</a>";
                                        echo "<form action='' method='POST' class='d-inline'>";
                                        echo "<button type='submit' name='delete_cat' value='{$user_id}' class='btn btn-danger btn-sm'>Delete</button>";
                                       echo "</form>";
                                    echo "</td>";
                                echo "</tr>";
                                        
                                    }

                                
                                ?>

                                <?php 
                                
                                if(isset($_POST['delete_user'])) {

                                    $the_user_id = mysqli_real_escape_string($con, $_POST['delete_user']);
                                
                                    $query = "DELETE FROM users WHERE user_id = '$the_user_id'";
                                    $query_run=mysqli_query($con, $query);
                                
                                    if($query_run) {
                                
                                        $_SESSION['message'] = "User Deleted Successfully";
                                        header("Location: view_all_users.php");
                                        exit(0);
                                
                                    } else { 
                                
                                        $_SESSION['message'] = "User Not Deleted";
                                        header("Location: view_all_users.php");
                                        exit(0);
                                
                                    }
                                
                                } 

                                ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include ('includes/footer.php'); ?>


